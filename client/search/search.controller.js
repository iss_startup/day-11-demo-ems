(function () {
    angular
        .module("EmployeeApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;


        vm.empName = '';
        vm.result = null;
        vm.showSalary = false;
        vm.names = [
            {
                displayName: "First Name",
                dbName: 'first_name'
            },
            {
                displayName: "Last Name",
                dbName: 'last_name'
            }
        ];

        vm.selectedName = vm.names[0];

        vm.search = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = false;
            vm.getEmployee(vm.empName, vm.selectedName.dbName)
                .then(function (response) {
                    console.info("success");
                    vm.result = response.data;
                }).catch(function (response) {
                    console.info("Error");
                    vm.status.message = "Failed to get the employee from the database.";
                    vm.status.code = 400;
                });
        };

        vm.searchForSalary = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = true;
            vm.getEmployeeSalary(vm.empName, vm.selectedName.dbName)
                .then(function (response) {
                    console.info("success");
                    vm.result = response.data;
                }).catch(function (response) {
                    console.info("Error");
                    vm.status.message = "Failed to get the employee from the database.";
                    vm.status.code = 400;
                });
        };

        vm.getEmployee = function (employeeName, employeeDBName) {
            return $http.get("/api/employee", {
                params: {'emp_name': employeeName, 'db_name': employeeDBName}
            });
        };

        vm.getEmployeeSalary = function (employeeName, employeeDBName) {
            return $http.get("/api/employeeSalary/", {
                params: {'emp_name': employeeName, 'db_name': employeeDBName}
            })
        };

    }
})();






