(function () {
    angular
        .module("EmployeeApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http"];

    function InsertCtrl($http) {
        var vm = this;
        vm.employee = {};
        vm.empNo = "";
        vm.employee.firstname = "";
        vm.employee.lastname = "";
        vm.employee.gender = "";
        vm.employee.birthday = "";
        vm.employee.hiredate = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.register = function () {
            vm.setEmployee(vm.employee)
                .then(function (response) {
                    console.info("success");
                    vm.status.message = "The employee is added to the database.";
                    vm.status.code = 202;
                }).catch(function (response) {
                    console.info("Error");
                    vm.status.message = "Failed to add the employee to the database.";
                    vm.status.code = 400;
                });
        };

        vm.setEmployee = function (employee) {
            return $http.post("/api/employee", employee);
        };
    }
})();






